from flask import render_template, flash, redirect,url_for,request,session
from Models.Models import db,User
from flask_login import login_required, logout_user, current_user
from werkzeug.security import generate_password_hash

class AccountController:
    @login_required
    def account():
        return render_template('accounts/index.html')
    
    @login_required
    def update_infos():
        email = request.form.get('email')
        name = request.form.get('name')
        
        if not name or not email:
            flash(message='Verifier vos informations !', category='error_infos')
            redirect(url_for('accountRoutes.account'))
            
        user = User.query.filter_by(email=current_user.email).first()
            
        if not user:
            flash('Votre token a expirer !', category='error_infos')
            logout_user()
            return redirect(url_for('authRoutes.login'))
        
        user.email = email
        user.name = name
        
        db.session().commit()
        
        session['name'] = user.name
        session['email'] = user.email
            
        
        flash(message='Informations modifiées !', category='success_infos')
        
        return redirect(url_for('accountRoutes.account'))
    
    
    @login_required
    def update_password():
        password = request.form.get('password')
        confirm_password = request.form.get('confirm_password')
        
        if password != confirm_password:
            flash(message='mots de passe different !', category='error_pwd')
            redirect(url_for('accountRoutes.account'))
            
        user = User.query.filter_by(email=current_user.email).first()
        
        if not user:
            flash('Votre token a expirer !', category='error_pwd')
            logout_user()
            return redirect(url_for('authRoutes.login'))
        
        user.password = generate_password_hash(password)
        
        db.session().commit()
        
        flash(message='Informations modifiées !', category='success_pwd')
        
        return redirect(url_for('accountRoutes.account'))