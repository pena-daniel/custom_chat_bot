from flask import render_template,flash,redirect, url_for, request
from flask_login import login_required, current_user
from Models.Models import ChatBot,db
import datetime

class ChatBotController:
    @login_required
    def me():
        per_page = int(request.args.get("per_page", 2))
        page = int(request.args.get("page", 1))
        bots = ChatBot.query.filter_by(user_id=current_user.id).order_by(ChatBot.created_at.asc()).paginate(page=page,per_page=per_page,error_out=False)
        return render_template('chatbots/index.html', bots=bots)
    
    @login_required
    def detail(id):
        bot = ChatBot.query.filter_by(id=id).first()
        return render_template('chatbots/detail.html', bot=bot)
    
    @login_required
    def update_view(id):
        bot = ChatBot.query.filter_by(id=id).first()
        return render_template('chatbots/update.html', bot=bot)
    
    @login_required
    def create():
        return render_template('chatbots/create.html')
    
    @login_required
    def store():
        name = request.form.get('name')
        description = request.form.get('description')
        
        new_chat = ChatBot(name=name, description=description)
        
        db.session.add(new_chat)
        db.session.commit()
        
        flash("Chatbot crée avec success !", category='success')
        
        return redirect(url_for('chatbots.me'))
    
    @login_required
    def update(id):
        name = request.form.get('name')
        is_block = False if request.form.get('is_block') is None else True
        description = request.form.get('description')
        
        print(request.form.get('is_block'))
        print(is_block)
        
        if not name or not description:
            flash(message='Verifier vos informations !', category='error')
            redirect(url_for('chatbots.create'))
            
        chat = ChatBot.query.filter_by(id=id).first()
            
        if not chat:
            flash('Votre token a expirer !', category='error')
            return redirect(url_for('chatbots.create'))
        
        chat.name = name
        chat.description = description
        chat.is_block = is_block
        chat.updated_at = datetime.datetime.now()
        
        db.session().commit()
            
        flash(message='Informations modifiées !', category='success')
        
        return redirect(url_for('chatbots.me'))
    
    @login_required
    def delete(id):
        bot = ChatBot.query.get_or_404(id)
        db.session.delete(bot)
        db.session.commit()
        flash(message='ChatBot supprimé avec success !', category='success')
        return redirect(url_for('chatbots.me'))
