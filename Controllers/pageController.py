from flask import render_template
from flask_login import login_required

class PageController:
    @login_required
    def index():
        return render_template('index.html')

    def about():
        return render_template('about.html')


        