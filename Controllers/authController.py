from flask import render_template,redirect, request, url_for,flash
from werkzeug.security import generate_password_hash, check_password_hash
from Models.Models import db,User
from flask_login import login_user,login_required, logout_user

class LoginController:
    def login():
        return render_template('auth/login.html')
    def handleLogin():
        # login code goes here
        email = request.form.get('email')
        password = request.form.get('password')
        remember = True if request.form.get('remember') else False

        user = User.query.filter_by(email=email).first()

        # check if the user actually exists
        # take the user-supplied password, hash it, and compare it to the hashed password in the database
        if not user or not check_password_hash(user.password, password):
            flash('Verifier vos informations et essayer à nouveau !', category='error')
            return redirect(url_for('authRoutes.login')) # if the user doesn't exist or password is wrong, reload the page

        login_user(user, remember=remember)
        # if the above check passes, then we know the user has the right credentials
        return redirect(url_for('chatbots.me'))

class RegisterController:
    
    def register():
        return render_template('auth/register.html')
    
    def handleRegister():
        email = request.form.get('email')
        name = request.form.get('name')
        password = request.form.get('password')

        user = User.query.filter_by(email=email).first()
        
        if user:
            flash("l'email existe déja !", category='error')
            return redirect(url_for('authRoutes.register'))
        
        new_user = User(email=email, name=name, password=generate_password_hash(password))
                
        # add the new user to the database
        db.session.add(new_user)
        db.session.commit()
        
        flash("Votre compte a été cree !", category='success')
        
        return redirect(url_for('authRoutes.login'))
    

class LogoutController:
    @login_required
    def logout():
        logout_user()
        return redirect(url_for('authRoutes.login'))