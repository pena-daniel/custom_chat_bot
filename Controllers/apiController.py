from flask import request,jsonify
from Models.Models import db,ChatBot
from services.Gemini import Gemini

class ApiController:
    
    def check(token):
        bot = ChatBot.query.filter_by(key=token).first()
        if bot.is_block:
            return jsonify({"message": "Token invalide!"}), 400
        return jsonify({"message": "Token valide!"}), 200
    
    def test():
        token = request.json['token']
        
        if not token:
            return jsonify({"message": "Token invalide!"}), 400
        
        return ApiRepository.handle(token,True)
        
    def message():
        token = request.json['token']
        
        if not token:
            return jsonify({"message": "Token invalide!"}), 400
        
        return ApiRepository.handle(token)
    
class ApiRepository:
    def handle(token, is_test = False):
        
        bot = ChatBot.query.filter_by(key=token).first()
                
        if not bot:
            return jsonify({"message": "Token invalide!"}), 400
        
        if bot.is_block:
            return jsonify({"message": "Chat bloquer!"}), 400
        
        if is_test and bot.test == 5:
            return jsonify({"message": "Impossible de tester vous avez atteint votre limite!"}), 400
        
        try:
            question = request.json['question']
            response = Gemini.predict(bot.description,question)
            
            if is_test:
                bot.test = bot.test + 1
                db.session.commit()

            
            return jsonify({"response": response}), 200
        except Exception as e:
            return jsonify({"message": "une erreur est survenue!"}), 500