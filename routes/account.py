from flask import Blueprint
from Controllers.accountController import AccountController

accountRoutes = Blueprint('accountRoutes', __name__)

accountRoutes.route('', methods=['GET'])(AccountController.account)
accountRoutes.route('/infos', methods=['POST'])(AccountController.update_infos)
accountRoutes.route('/password', methods=['POST'])(AccountController.update_password)