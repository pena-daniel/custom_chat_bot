from flask import Blueprint
from Controllers.chatBotController import ChatBotController

chatbots = Blueprint('chatbots', __name__)

chatbots.route('/me', methods=['GET'])(ChatBotController.me)
chatbots.route('/detail/<int:id>', methods=['GET'])(ChatBotController.detail)
chatbots.route('/update/<int:id>', methods=['GET'])(ChatBotController.update_view)
chatbots.route('/create', methods=['GET'])(ChatBotController.create)
chatbots.route('/store', methods=['POST'])(ChatBotController.store)
chatbots.route('/update/<int:id>', methods=['POST'])(ChatBotController.update)
chatbots.route('/delete/<int:id>', methods=['POST'])(ChatBotController.delete)