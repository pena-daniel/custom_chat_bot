from flask import Blueprint
from Controllers.apiController import ApiController

api = Blueprint('api', __name__)

api.route('/check/<token>', methods=['GET'])(ApiController.check)
api.route('/test', methods=['POST'])(ApiController.test)
api.route('/message', methods=['POST'])(ApiController.message)
