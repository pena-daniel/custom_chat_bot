from flask import Blueprint
from Controllers.authController import LoginController,RegisterController, LogoutController

authRoutes = Blueprint('authRoutes', __name__)

authRoutes.route('/login', methods=['GET'])(LoginController.login)
authRoutes.route('/login', methods=['POST'])(LoginController.handleLogin)

authRoutes.route('/register', methods=['GET'])(RegisterController.register)
authRoutes.route('/register', methods=['POST'])(RegisterController.handleRegister)

authRoutes.route('/logout', methods=['GET'])(LogoutController.logout)