from flask import Blueprint
from Controllers.pageController import PageController

pagesRoutes = Blueprint('pagesRoutes', __name__)

pagesRoutes.route('', methods=['GET'])(PageController.index)
pagesRoutes.route('about', methods=['GET'])(PageController.about)