import os

APP_NAME="My CHATBOT"
DEBUG = True
PORT = 5001
SECRET_KEY = os.urandom(32)
    
basedir = os.path.abspath(os.path.dirname(__file__))
    
# Connect to the database
SQLALCHEMY_DATABASE_URI = 'sqlite:///'+os.path.join(basedir,'my_chat_bot.db')
    
basedir = os.path.abspath(os.path.dirname(__file__))

# Turn off the Flask-SQLAlchemy event system and warning
SQLALCHEMY_TRACK_MODIFICATIONS = False