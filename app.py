from flask import Flask
from flask_migrate import Migrate
from flask_login import LoginManager

from flask_cors import CORS

from routes.auth import authRoutes
from routes.account import accountRoutes
from routes.page import pagesRoutes
from routes.chatbots import chatbots
from routes.api import api

from Models.Models import db, User

def create_app():
    app = Flask(__name__)  # flask app object
    app.config.from_object('config')  # Configuring from Python Files
    return app

app = create_app()  # Creating the app
db.init_app(app)
login_manager = LoginManager()
login_manager.login_view = 'authRoutes.login'
login_manager.init_app(app)
migrate = Migrate(app, db)

CORS(app, resources={r"/api/*": {"origins": "*"}})

@login_manager.user_loader
def load_user(user_id):
    # since the user_id is just the primary key of our user table, use it in the query for the user
    return User.query.get(int(user_id))

# Registering the blueprint
app.register_blueprint(pagesRoutes, url_prefix='/')
app.register_blueprint(authRoutes, url_prefix='/auth')
app.register_blueprint(accountRoutes, url_prefix='/account')
app.register_blueprint(chatbots, url_prefix='/chat')
app.register_blueprint(api, url_prefix='/api')

if __name__ == "__main__":
    debug=True
    app.run(port=5001)