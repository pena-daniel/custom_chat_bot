
/**
 *@author pena (devDan)
 *
 * @export
 * @class ChatBox
 * @extends {HTMLElement}
 *@param {ShadowRoot} root
 */
class ChatBox extends HTMLElement{

    sendUrl = null;
    container = null;
    wall = null;
    token = null;
    isTest = false;
    state = false;
    testCount = 0;
    valueCount = 0;
    messages = [];
    

    constructor(){
        super();
        // this.root = this.attachShadow({mode: 'open'});
    }

    connectedCallback(){
        const div = this.render()

       

        this.insertAdjacentElement('afterend', div)

        this.container = div
        this.wall = this.container.querySelector('.wall')
    }


    getAttributes(){
        return {
            placeHolder: this.getAttribute('placeHolder') || 'Votre message',
            isTest: this.getAttribute('isTest') || false,
            sendUrl: this.getAttribute('sendUrl') || null,
            testCount: this.getAttribute('testCount') || 0,
            token: this.getAttribute('token') || null,
            valueCount: this.getAttribute('valueCount') || 0,
        }
    }

    render() {

        const { placeHolder, isTest, sendUrl, testCount, valueCount, token } = this.getAttributes()
        this.placeHolder = placeHolder
        this.isTest = isTest
        this.sendUrl = sendUrl
        this.testCount = testCount
        this.valueCount = valueCount
        this.token = token


        const indicator = this.isTest ? `
            <div class="indicator">
                ${this.valueCount} / ${this.testCount}
            </div>
        ` : ""
        const dom = `
            <div class="message__box">
                <div>
                    ${indicator}
                </div>
                <div class="wall">
                
                </div>
                <div class="form-group sender__form">
                    <div>
                        <input type="text" name="message" id="message" placeholder="Votre message">
                    </div>
                    <button class="btn btn-main" type="submit">Envoyer</button>
                </div>
            </div>
        `;

        const element = this.generateElement(dom);

        element.querySelector('button').addEventListener('click', this.onSendMessage.bind(this))
        element.querySelector('input').addEventListener('keyup', (e) => {
            e.preventDefault()
            if (e.code == 'Enter' || e.keyCode == 13) {
                this.onSendMessage()
            }
        })
    
        return element;
        
    }

    onSendMessage(){
        if (this.state) {
            alert("Une requettes est deja en cours !")
            return
        }
        if (this.valueCount == this.testCount) {
            alert("Vos test sont terminés !")
            return
        }
        const val = this.container.querySelector('input').value

        if (val.length == 0){
            alert("Veuillez entrez une question s'il vous plait !")
            return
        }

        this.container.querySelector('input').value = ""

        this.state = true
        this.onNewMessage({
            incomming: false,
            content: val
        })
    }

    /**
     * 
     * @param {Objects} message 
     */
    onNewMessage(message){
        this.messages.push(message)

        // render the message
        if (this.container === null || this.messages.length == 0) return

        const msg = this.generateMessage(this.messages.length, message)
        this.wall.appendChild(msg)
        this.wall.scrollTop = this.wall.scrollHeight
        this.sendMessageRequest(message.content)
    }

    /**
     * 
     * @param {Objects} message 
     */
    generateMessage(id, message){
        const msg = `
            <div class="msg ${message.incomming ? 'incomming' : 'outgoing'}" id="msg-${id}">
                <div class="profil">${message.incomming ? '🤖' : '🧑‍💻'}</div>
                <p>${message.content}</p>
            </div>
        `;

        return this.generateElement(msg);
    }

    /**
     * 
     * @param {String} str 
     * @return Element | null
     */
    generateElement(str){
        return document.createRange().createContextualFragment(str).firstElementChild
    }


    async sendMessageRequest(msg){
        this.messages.push({
            incomming: true,
            content: ""
        })
        const id = this.messages.length;
        const r_div = this.generateMessage(id, {
            incomming: true,
            content: ""
        })
        this.wall.appendChild(r_div)
        this.wall.scrollTop = this.wall.scrollHeight
        this.messages[id-1].content = "Attendez que l'on vous reponde..."
        this.writeText(id, "Attendez que l'on vous reponde...")

        try {
            // TODO: send the api request
            const request = await fetch("http://127.0.0.1:5000"+this.sendUrl, {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    "question": msg,
                    "token": this.token,
                })
            })
            // TODO: get the api response
            const response = await request.json()
            // TODO: display the response properly
            let res = ![500, 400].includes(request.status) ? response.response : response.message
            this.state = false
            if (request.status == 200) {
                this.valueCount++
                this.container.querySelector('.indicator').innerHTML = `${this.valueCount} / ${this.testCount}`
                this.messages[id-1].content = res
                setTimeout(() => {
                    this.writeText(id, res)
                },35*50)
            }else{
                this.messages[id-1].content = res
                setTimeout(() => {
                    this.writeText(id, res)
                },35*50)
            }
        } catch (error) {
            this.state = false
            this.messages[id-1].content = "Une erreur est survenue verifiez votre connexion internet !"
            setTimeout(() => {
                this.writeText(id, "Une erreur est survenue verifiez votre connexion internet !", 40)
            },35*50)
        }
    }

    writeText (id_item, msg, time=50, span=null){
        let item = document.querySelector(`#msg-${id_item} p`)
        let i = 0
        item.innerHTML = ""
        const intervale = setInterval(
            () => {
                item.innerHTML += msg[i]
                this.wall.scrollTop = this.wall.scrollHeight
                if (i == msg.length-1) {
                    clearInterval(intervale)
                    if (span != null) {
                        item.innerHTML += span.span
                        this.wall.scrollTop = this.wall.scrollHeight
                        span.res.forEach((intent, index) => {
                            item.querySelector(`#${id_item}b${index+1}`).addEventListener(
                                'click',(e) => {
                                    writeText(id_item, intent.response)
                            })
                        });
                    }
                }
                i++
            },
            time
        )
    }


}


customElements.define('chat-box', ChatBox);
