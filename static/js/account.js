const menu = document.querySelector('.menu ul')
const infosMenu = document.querySelector('.infos-menu')
const mdpMenu = document.querySelector('.mdp-menu')
const infosForm = document.querySelector('.infos')
const pwdForm = document.querySelector('.pwd')


infosMenu.classList.add("active")
infosForm.classList.add("show")
menu.classList.add("left")


infosMenu.addEventListener('click', () => {
    mdpMenu.classList.remove("active")
    infosMenu.classList.add("active")

    menu.classList.remove("right")
    menu.classList.add("left")

    pwdForm.classList.remove('show')
    infosForm.classList.add('show')
})
mdpMenu.addEventListener('click', () => {
    infosMenu.classList.remove("active")
    mdpMenu.classList.add("active")

    menu.classList.remove("left")
    menu.classList.add("right")

    infosForm.classList.remove('show')
    pwdForm.classList.add('show')
})
