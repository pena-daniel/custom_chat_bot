from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from sqlalchemy.sql import func
import os
from flask_login import current_user
import secrets

db = SQLAlchemy()

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(120))
    models = db.relationship('ChatBot', back_populates='user')
    created_at = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    
    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
        }



class ChatBot(db.Model):
    __tablename__ = 'chat_bot'
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    key = db.Column(db.String)
    test = db.Column(db.Integer, default=0)
    is_block = db.Column(db.Boolean, default=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User', back_populates='models')
    description = db.Column(db.Text, default="")
    created_at = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    updated_at = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())
    
    def __init__(self, name, description="", is_block=False):
        self.key = secrets.token_urlsafe(20)
        self.user_id = current_user.id
        self.name = name
        self.description = description
        self.is_block = is_block
            
    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'user': self.user,
        }